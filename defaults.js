
function uniqueKeys(obj, defaultParams) {
    var k = []
    for (const key in obj) {
        if (key in defaultParams) {
            k.push(key)
        }
    }
    return k;
}
function defaults(obj, defaultParams) {
    if(!obj) return;
    if (obj != {}) {
        var unique = uniqueKeys(obj, defaultParams);
        for (i = 0; i < unique.length; i++) {
            defaultParams[unique[i]] = obj[unique[i]]
        }
    }
    return defaultParams;
}

module.exports = {
    defaults,
};

