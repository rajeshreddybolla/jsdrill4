

function pairs(obj) {
    let arr = [];
    let result = [];

    if(!obj) return null;

    for (const key in obj) {

        arr.push(key);
        arr.push(obj[key]);

    }
    for (var i = 0; i < arr.length; i += 2) {
        result.push(arr.slice(i, i + 2));
    }

    return result;
}

module.exports = {
    pairs,
};
