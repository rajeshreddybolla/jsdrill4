
function mapObject(obj, cb) {
  if (!obj) return null;
  let result = {};
  for (const value in obj) {
    result[value] = cb(obj[value]);
  }
  return result
}

module.exports = {
  mapObject,
};

