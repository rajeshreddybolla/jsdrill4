
function invert(obj) {

    let result = {};
   
    if(!obj){
        return null;
    }

    for (const key in obj) {
        result[obj[key]] = key ;
    }
    return result;
}

module.exports = {
    invert,
};