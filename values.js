
function values(obj) {

    let result = [];

    if (!obj || Array.isArray(obj)) {
        return result;
    }

    for (const value in obj) {
            result.push(obj[value]);
    }
    return result;
}


module.exports = {
    values,
};