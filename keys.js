
function keys(obj) {

    let result = [];

    if (!obj || Array.isArray(obj)) {
        return result;
    }

    for (const key in obj) {
        result.push(key);
    }
    return result;
}


module.exports = {
    keys,
};